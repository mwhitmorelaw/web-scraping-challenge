from flask import Flask, render_template
import scrape_mars
import pymongo

app = Flask(__name__)

@app.route('/')
def index():
  # elements = scrape_mars.scrape()
  # return render_template('index.html', elements)
  return render_template('index.html', elements={
    'news_title': 'title here',
    'news_p': 'content here', 
    'hemisphere_image_urls': [
        {"title": "Valles Marineris Hemisphere", "img_url": "..."},
        {"title": "Cerberus Hemisphere", "img_url": "..."},
        {"title": "Schiaparelli Hemisphere", "img_url": "..."},
        {"title": "Syrtis Major Hemisphere", "img_url": "..."},
    ],
  })


@app.route('/scrape')
def scrape_and_store():
  d = scrape_mars.scrape()
  db = client.craigslist_db
  collection = db.mars_data
  collection.insert_one(d)

conn = 'mongodb://localhost:27017'
client = pymongo.MongoClient(conn)

if __name__ == '__main__':
  app.run(debug=True)
