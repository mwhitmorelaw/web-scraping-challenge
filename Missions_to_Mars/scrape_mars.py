import pandas as pd
from splinter import Browser
from bs4 import BeautifulSoup

def scrape():
  elements = {}

  nasa_url = 'https://mars.nasa.gov/news/?page=0&per_page=40&order=publish_date+desc%2Ccreated_at+desc&search=&category=19%2C165%2C184%2C204&blank_scope=Latest'
  executable_path = {'executable_path': './chromedriver.exe'}
  browser = Browser('chrome', **executable_path, headless=False)
  browser.visit(nasa_url)

  html = browser.html
  soup = BeautifulSoup(html, 'html.parser')

  title_class = 'content_title'
  body_class = 'rollover_description_inner'

  title_elements = soup.find_all('div', class_=title_class)
  body_elements = soup.find_all('div', class_=body_class)

  elements['news_title'] = title_elements[0].text.replace('\n', '')
  elements['news_p'] = body_elements[0].text.replace('\n', '')

  elements['featured_image_url'] = 'https://www.jpl.nasa.gov/spaceimages/images/wallpaper/PIA18452-1920x1200.jpg'

  twitter_url = 'https://twitter.com/marswxreport?lang=en'
  browser.visit(twitter_url)
  html = browser.html
  soup = BeautifulSoup(html, 'html.parser')

  text_elements = soup.find_all('div',class_="css-901oao r-hkyrab r-1qd0xha r-a023e6 r-16dba41 r-ad9z0x r-bcqeeo r-bnwqim r-qvutc0")
  elements['mars_weather'] = text_elements[0].text

  mars_facts_url = 'https://space-facts.com/mars/'
  mars_facts = pd.read_html(mars_facts_url)[1].to_html()

  return elements
